//alias for backwards compat
export { ThemeProvider, ThemeProvider as ThemeCompositions } from "./theme-provider"
export * as classes from "./theme.module.scss"
export type { Theme } from "./theme"
export type { ThemeProviderProps } from "./theme-provider"
