import { RxmodelAspect } from './rxmodel.aspect';

export type { RxmodelMain } from './rxmodel.main.runtime';
export default RxmodelAspect;
export { RxmodelAspect };
