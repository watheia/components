import { MainRuntime } from '@teambit/cli';
import { RxmodelAspect } from './rxmodel.aspect';

export class RxmodelMain {
  static slots = [];
  static dependencies = [];
  static runtime = MainRuntime;
  static async provider() {
    return new RxmodelMain();
  }
}

RxmodelAspect.addRuntime(RxmodelMain);
