import React from 'react';
import type { NavLinkProps } from '@watheia/layout.routing.native-nav-link';
import { useRouting } from '@watheia/layout.routing.routing-provider';

export type { NavLinkProps };

export function NavLink(props: NavLinkProps) {
	const ActualNavLink = useRouting().NavLink;
	return <ActualNavLink {...props} />;
}
